// src/store/index.ts
import { defineStore } from 'pinia';
import { createPinia } from 'pinia';
// import piniaPersist from 'pinia-plugin-persist';


// defineStore 调用后返回一个函数，调用该函数获得 Store 实体
export const useGlobalStore = defineStore({
    id: 'GlobalState', // id: 必须的，在所有 Store 中唯一
    // state: 返回对象的函数
    state: (): any => ({
        token:'tasjhdkasjldasda'
    }),
    getters: {},
    actions: {

    },
    // 持久化
    // persist: {
    //     enabled: true,
    //     strategies: [
    //         {
    //             key: 'GlobalState',
    //             storage: localStorage,
    //             // storage: sessionStorage,
    //         }
    //     ]
    // }
});

const pinia = createPinia();

export default pinia;