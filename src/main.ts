import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import "./assets/style/global.less";//全局样式
import pinia from "@/store/index";

createApp(App).use(router).use(pinia).mount('#app')
